package problem125_Valid_Palindrome;

public class Solution {
	 public boolean isPalindrome(String s) {
		 if("".equals(s))	return true;
		 char[] str=s.toLowerCase().toCharArray();
		 int i=0,j=str.length-1;
		 while(i<str.length&&j>=0&&i<=j){
			 while(i<str.length&&!isAlphanumeric(str[i])){
				 i++;
			 }
			 while(j>=0&&!isAlphanumeric(str[j])){
				 j--;
			 }
			 if(i<str.length&&j>=0){
				 if(str[i]!=str[j])
					 return false;
			 }
			 i++;j--;
		 }
		 return true;
	 }
	 
	 private boolean isAlphanumeric(char c){
		 return (c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c>='0'&&c<='9');
	 }
}
