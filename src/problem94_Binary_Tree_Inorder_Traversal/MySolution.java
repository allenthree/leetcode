package problem94_Binary_Tree_Inorder_Traversal;

import java.util.LinkedList;
import java.util.List;

public class MySolution {
	    public List<Integer> inorderTraversal(TreeNode root) {
	        List<Integer> result=new LinkedList<>();
	        inorder(root,result);
	        return result;
	    }
	    
	    private void inorder(TreeNode root,List<Integer> list){
	    	if(root!=null){
	    		inorder(root.left,list);
	    		list.add(root.val);
	    		inorder(root.right,list);
	    	}
	    }
	    
	    public class TreeNode {
		     public int val;
		     public TreeNode left;
		     public TreeNode right;
		     public TreeNode(int x) { val = x; }
		 }
	}