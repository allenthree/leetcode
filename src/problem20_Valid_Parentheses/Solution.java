package problem20_Valid_Parentheses;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Solution {
	private static final Map<Character,Character> parentheses=new HashMap<>();
	static{
		parentheses.put('}', '{');
		parentheses.put(']','[');
		parentheses.put(')','(');
	}
	public boolean isValid(String s) {
		Stack<Character> stack=new Stack<>();
		char[] chars=s.toCharArray();
		for(char c:chars){
			if(parentheses.containsValue(c)){
				stack.push(c);
			}else{
				if(stack.isEmpty()||parentheses.get(c)!=stack.peek())
					return false;
				else
					stack.pop();
			}
		}
		if(stack.isEmpty())
			return true;
		return false;
	}
}
