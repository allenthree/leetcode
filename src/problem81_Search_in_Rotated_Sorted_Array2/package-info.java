/**
 * Follow up for "Search in Rotated Sorted Array":
 * What if duplicates are allowed?
 * Would this affect the run-time complexity? How and why?
 * Write a function to determine if a given target is in the array.
 */
/**
 * @author Ivan
 */
package problem81_Search_in_Rotated_Sorted_Array2;