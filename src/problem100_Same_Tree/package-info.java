/**
 * Given two binary trees, write a function to check if they are equal or not.
 * Two binary trees are considered equal if they are structurally identical and the nodes have the same value.
 */
/**
 * @author Ivan
 *
 */
package problem100_Same_Tree;