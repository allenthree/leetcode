package problem100_Same_Tree;

import problem95_UniqueBinarySearchTreesII.MySolution.TreeNode;

public class MySolution {
	public boolean isSameTree(TreeNode p, TreeNode q) {
		return (p==null&&q==null)?true:(p==null||q==null?false:p.val==q.val&&isSameTree(p.left,q.left)&&isSameTree(p.right,q.right));
    }
}
