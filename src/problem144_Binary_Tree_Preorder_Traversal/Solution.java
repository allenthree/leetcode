package problem144_Binary_Tree_Preorder_Traversal;

import java.util.LinkedList;
import java.util.List;

import problem105_Construct_Binary_Tree_from_Preorder_and_Inorder_Traversal.MySolution.TreeNode;

public class Solution {
	 public List<Integer> preorderTraversal(TreeNode root) {
	     List<Integer> result=new LinkedList<>();
	     preorder(root,result);
	     return result;
	 }
	 
	 private void preorder(TreeNode root,List<Integer> list){
		 if(root!=null){
			 list.add(root.val);
			 preorder(root.left,list);
			 preorder(root.right,list);
		 }
	 }
}
