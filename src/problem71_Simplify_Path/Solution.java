package problem71_Simplify_Path;

import java.util.Stack;

public class Solution {
	 public String simplifyPath(String path) {
		 Stack<String> stack=new Stack<>();
		 String[] entries=path.split("/");
		 for(String entry:entries){
			 if(entry.equals("..")){
				 if(!stack.isEmpty()) stack.pop();
			 }else if(!entry.equals(".")&&!entry.equals("")){
				 stack.push(entry);
			 }
		 }
		 if(stack.isEmpty()) return "/";
		 String result="";
		 while(!stack.isEmpty()){
			 result="/"+stack.pop()+result;
		 }
		 return result;
	 }
	 
	 public static void main(String[] args){
		 System.out.println(new Solution().simplifyPath("/abc/..."));
		 System.out.println(new Solution().simplifyPath("/../"));
		 System.out.println(new Solution().simplifyPath("/a/./b/../../c/b"));
	 }
	 
}
