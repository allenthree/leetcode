/**
 * Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers 
 * in this range, inclusive.For example, given the range [5, 7], you should return 4.
 */
/**
 * @author I321035
 *
 */
package problem201_Bitwise_AND_of_Numbers_Range;