package problem201_Bitwise_AND_of_Numbers_Range;

/**
 * [26,30]
 * 11010　　11011　　11100　　11101　　11110
 * 可以发现与结果即为这些数字的最左边的公共部分。
 * 
 * @author I321035
 *
 */

public class Solution {
	  public int rangeBitwiseAnd(int m, int n) {
	      int i=0;  
		  while(m!=n){
	        	m>>=1;
	        	n>>=1;
	        	i++;
	      }
		  return m<<i;
	  }
}
