/**
 * Given a binary tree, return the postorder traversal of its nodes' values.

For example:
Given binary tree {1,#,2,3},
   1
    \
     2
    /
   3
return [3,2,1].
 */
/**
 * @author I321035
 *
 */
package problem145_Binary_Tree_Postorder_Traversal;