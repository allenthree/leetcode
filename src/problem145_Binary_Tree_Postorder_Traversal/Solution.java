package problem145_Binary_Tree_Postorder_Traversal;

import java.util.LinkedList;
import java.util.List;

import problem105_Construct_Binary_Tree_from_Preorder_and_Inorder_Traversal.MySolution.TreeNode;

public class Solution {
	 public List<Integer> postorderTraversal(TreeNode root) {
		  List<Integer> result=new LinkedList<>();
		     postorder(root,result);
		     return result;
	 }
	 
	 private void postorder(TreeNode root,List<Integer> list){
		 if(root!=null){
			 postorder(root.left,list);
			 postorder(root.right,list);
			 list.add(root.val);
		 }
	 }
}
