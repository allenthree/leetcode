/**
 * Two elements of a binary search tree (BST) are swapped by mistake.Recover the tree without changing its structure.
 */
/**
 * @author Ivan
 *
 */
package problem99_Recover_Binary_Search_Tree;