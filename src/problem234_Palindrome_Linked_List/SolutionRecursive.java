package problem234_Palindrome_Linked_List;

import problem19_RemoveNthNodeFromEndofList.Solution.ListNode;

public class SolutionRecursive {
	private ListNode left;

	public boolean isPalindrome(ListNode head) {
		left = head;
		return dfs(head);
	}

	private boolean dfs(ListNode right) {
		if (right == null)
			return true;
		if (!dfs(right.next))
			return false;
		boolean y = left.val == right.val;
		left = left.next;
		return y;
	}
}
