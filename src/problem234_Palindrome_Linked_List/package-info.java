/**
 * Given a singly linked list, determine if it is a palindrome.
 * Follow up:
 * Could you do it in O(n) time and O(1) space?
 */
/**
 * @author I321035
 *
 */
package problem234_Palindrome_Linked_List;