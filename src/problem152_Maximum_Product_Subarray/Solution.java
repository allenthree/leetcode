package problem152_Maximum_Product_Subarray;

public class Solution {
	public int maxProduct(int[] nums) {
		if(nums.length==0)	return 0;
		int maxLocal=nums[0],minLocal=nums[0],max=nums[0];
		for(int i=1;i<nums.length;i++){
			int a =maxLocal*nums[i];
			int b=minLocal*nums[i];
			maxLocal=Math.max(Math.max(a, b), nums[i]);
			minLocal=Math.min(Math.min(a, b), nums[i]);
			max=Math.max(max, maxLocal);
		}
		return max;
    }
}
