/**
 * Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes v
 * and w as the lowest node in T that has both v and w as descendants (where we allow a node to be a descendant 
 * of itself).”
 */
/**
 * @author I321035
 *
 */
package problem235_Lowest_Common_Ancestor_of_a_Binary_Search_Tree;