package problem235_Lowest_Common_Ancestor_of_a_Binary_Search_Tree;

import problem105_Construct_Binary_Tree_from_Preorder_and_Inorder_Traversal.MySolution.TreeNode;

public class Solution {
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		if((p.val-root.val)*(q.val-root.val)<=0)
			return root;
		else if(p.val<root.val&&q.val<root.val)
			return lowestCommonAncestor(root.left,p,q);
		return lowestCommonAncestor(root.right,p,q);
	}
}
