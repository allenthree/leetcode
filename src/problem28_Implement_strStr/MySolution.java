package problem28_Implement_strStr;

public class MySolution {
	public int strStr(String haystack, String needle) {
		if("".equals(needle)) return 0;
		//return haystack.indexOf(needle);
		char[] src=haystack.toCharArray();
		char[] target=needle.toCharArray();
		int i=0,j=0;
		while(i<src.length){
			int start=0;
			while(i<src.length&&src[i]!=target[0]){
				i++;
			}
			start=i;
			while(i<src.length&&j<target.length&&src[i]==target[j]){
				i++;
				j++;
			}
			if(j==target.length)
				return start;
			else{
				j=0;
				i=start+1;
			}
		}
		return -1;
	}
	
	public static void main(String[] args){
		System.out.println("".indexOf(""));
	}
}
