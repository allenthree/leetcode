package problem65_Valid_Number;


public class MySolution {
	private static final int[][] trans=new int[][]{
		{-1,  0,  1,  2, -1,  3},
        {-1, -1, -1,  2, -1,  3},
        {-1, -1, -1, -1, -1,  4},
        {-1,  5, -1,  4,  6,  3},
        {-1,  5, -1, -1,  6,  4},
        {-1,  5, -1, -1, -1, -1},
        {-1, -1,  7, -1, -1,  8},
        {-1, -1, -1, -1, -1,  8},
        {-1,  5, -1, -1, -1,  8}
    };
	
	private static enum Input{
		INVALID,SPACE,SIGN,DOT,E,DIGIT;
	}
	
	
	 public boolean isNumber(String s) {
		 int state=0;Input input=Input.INVALID;
		 char[] arr=s.toCharArray();
		 for(char c:arr){
			 if(c==' '){
				 input=Input.SPACE;
			 }else if(c=='+'||c=='-'){
				 input=Input.SIGN;
			 }else if(c=='.'){
				 input=Input.DOT;
			 }else if(c=='e'){
				 input=Input.E;
			 }else if(Character.isDigit(c)){
				 input=Input.DIGIT;
			 }else{
				 input=Input.INVALID;
			 }
			 state=trans[state][input.ordinal()];
			 if(state==-1)
				 return false;
		 }
		 return state == 3 || state == 4 || state == 5 || state == 8;
	 }
	 
	 public static void main(String[] args){
		 MySolution my=new MySolution();
		 System.out.println(my.isNumber("e10"));
	 }
}
