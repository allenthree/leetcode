/**
 * Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.
 */
/**
 * @author I321035
 *
 */
package problem230_Kth_Smallest_Element_in_a_BST;