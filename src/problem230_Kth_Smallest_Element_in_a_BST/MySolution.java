package problem230_Kth_Smallest_Element_in_a_BST;

import java.util.Stack;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;


public class MySolution {
	public int kthSmallest(TreeNode root, int k) {
		int count=0;
		Stack<TreeNode> stack=new Stack<>();
		while(true){
			while(root!=null){
				stack.push(root);
				root=root.left;
			}
			if(!stack.isEmpty()){
				TreeNode curr=stack.pop();
				if(++count==k){
					return curr.val;
				}
				root=curr.right;
			}else{
				break;
			}
		}
		return -1;
    }
	
	public static void main(String[] args){
		System.out.println(new MySolution().kthSmallest(new TreeNode(1), 1));
	}
}
