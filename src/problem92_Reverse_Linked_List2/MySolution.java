package problem92_Reverse_Linked_List2;

import problem82_Remove_Duplicates_from_Sorted_List2.ListNode;

public class MySolution {
	public ListNode reverseBetween(ListNode head, int m, int n) {
        if(m==n)    return head;
         ListNode dummy=new ListNode(0);
         dummy.next=head;int i=0;
         ListNode pre=dummy;
         while(++i<m){
             pre=pre.next;
         }
         ListNode curr=pre.next;
         int j=i;
         while(j++<n){
             ListNode tmp=curr.next.next;
             curr.next.next=pre.next;
             pre.next=curr.next;
             curr.next=tmp;
         }
         return dummy.next;
    }
}
