package problem114_Flatten_Binary_Tree_to_Linked_List;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class Solution {
	 public void flatten(TreeNode root) {
	       while(root!=null){  
	            if(root.left!=null){  
	                TreeNode pre = root.left;  
	                while(pre.right!=null)  
	                    pre = pre.right;  
	                pre.right = root.right;  
	                root.right = root.left;  
	                root.left = null;  
	            }  
	            root = root.right;  
	        }   
	    }
}
