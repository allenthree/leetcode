package problem257_Binary_Tree_Paths;

import java.util.LinkedList;
import java.util.List;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class Solution {
	private List<String> result=new LinkedList<>();
	
	 public List<String> binaryTreePaths(TreeNode root) {
		 dfs(root,"");
		 return result;
	 }
	 
	 private void dfs(TreeNode node,String current){
		 if(node==null) return;
		 current+=node.val;
		 if(node.left==null&&node.right==null)
			 result.add(current);
		 current+="->";
		 dfs(node.left,current);
		 dfs(node.right,current);
	 }
	 
	 public static void main(String[] args){
		 System.out.println(~(8-1));
		 
		 TreeNode root=new TreeNode(1);
		 root.right=new TreeNode(3);
		 TreeNode l=new TreeNode(2);
		 root.left=l;
		 l.right=new TreeNode(5);
		 l.left=new TreeNode(4);
		 System.out.println(new Solution().binaryTreePaths(root));
	 }
}
