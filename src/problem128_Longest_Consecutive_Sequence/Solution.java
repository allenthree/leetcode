package problem128_Longest_Consecutive_Sequence;

import java.util.HashSet;
import java.util.Set;

public class Solution {
	public int longestConsecutive(int[] nums) {
		Set<Integer> allNums=new HashSet<>();
		for(int n:nums){
			allNums.add(n);
		}
		Set<Integer> visited=new HashSet<>();
		int max=0;
		for(int n:nums){
			if(visited.contains(n))
				continue;
			int currentLength=0;
			int curr=n;
			//find n+1,n+2....
			while(allNums.contains(n)){
				visited.add(n);
				currentLength++;
				n++;
			}
			//find n-1,n-2,
			while(allNums.contains(curr-1)){
				visited.add(curr-1);
				currentLength++;
				curr--;
			}
			max=Math.max(max, currentLength);
		}
		return max;
    }
	
	public static void main(String[] args){
		System.out.println(new Solution().longestConsecutive(new int[]{100, 4, 200, 1, 3, 2}));
	}
}
