package problem166_Fraction_to_Recurring_Decimal;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public static void main(String[] args){
		System.out.println(new Solution().fractionToDecimal(Integer.MIN_VALUE, -1));
	}
	
	public String fractionToDecimal(int numerator, int denominator) {
		long num=numerator,den=denominator;
		 boolean neg=numerator>0&&denominator<0||numerator<0&&denominator>0;
		 num=Math.abs(num);
		 den=Math.abs(den);
		 String res=neg?"-"+Long.toString(num/den):Long.toString(num/den);
		 long remainder=num%den;
		 return remainder==0?res:res+"."+getDec(remainder,den);
	}
	
	private String getDec(long remainder, long den) {  
		StringBuilder sb=new StringBuilder();
		Map<Long,Integer> map=new HashMap<>();
		int i=0;
		while(remainder!=0&&!map.containsKey(remainder)){
			map.put(remainder, i);
			sb.append(remainder*10/den);
			remainder=remainder*10%den;
			i++;
		}
		if(remainder!=0){
			sb.insert(map.get(remainder), "(");
			sb.append(")");
		}
		return sb.toString();
	}
}
