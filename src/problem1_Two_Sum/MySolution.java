package problem1_Two_Sum;

import java.util.Arrays;

public class MySolution {
	public int[] twoSum(int[] nums, int target) {
		int[] result = { 0, 0 };
		for (int i = 0; i < nums.length; i++) {
			result[0] = i;
			int searchTarget = target - nums[i];
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[j] == searchTarget) {
					result[1] = j;
					return result;
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		MySolution solution = new MySolution();
		System.out.println(Arrays.toString(solution.twoSum(new int[] { 2, 7, 11, 15 }, 9)));
	}
}
