package problem260_Single_Number_III;

public class Solution {
	public int[] singleNumber(int[] nums) {
		int[] res=new int[2];
		int xor=0;
		for(int n:nums)
			xor^=n;
		//xor=res[0]^res[1]
		int low=xor&(0-xor); //获得最低位的1
		for(int n:nums){
			if((n&low)!=0)	res[0]^=n;
		}
		res[1]=res[0]^xor;
		return res;
    }
	
	public static void main(String[] args){
		System.out.println(new Solution().singleNumber(new int[]{1,1,2,2,3,5}));
	}
}
