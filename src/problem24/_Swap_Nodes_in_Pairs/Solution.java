package problem24._Swap_Nodes_in_Pairs;


public class Solution {
	public class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}
	public ListNode swapPairs(ListNode head) {
		ListNode dummy=new ListNode(-1);
		dummy.next=head;
		ListNode last=dummy;
		while(last.next!=null&&last.next.next!=null){
			ListNode n1=last.next,n2=last.next.next;
			last.next=n2;
			n1.next=n2.next;
			n2.next=n1;
			last=n1;
		}
		return dummy.next;
	}
}
