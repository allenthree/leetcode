/**
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 * Return all possible palindrome partitioning of s.
 * For example, given s = "aab",
 * Return
 * [
 * 	["aa","b"],
 *  ["a","a","b"]
 * ]
 */
/**
 * @author Ivan
 *
 */
package problem131_Palindrome_Partitioning;