package problem129_Sum_Root_to_Leaf_Numbers;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class MySolution {
	public int sumNumbers(TreeNode root) {
        return dfs(root,0);
    }
	
	private int dfs(TreeNode root,int result){
		if(root==null)
			return 0;
		if(root.left==null&&root.right==null)
			return result*10+root.val;
		return dfs(root.left,result*10+root.val)+dfs(root.right,result*10+root.val);
	}
	
	public static void main(String[] args){
		TreeNode root=new TreeNode(0);
		root.left=new TreeNode(1);
		System.out.println(new MySolution().sumNumbers(root));
	}
}
