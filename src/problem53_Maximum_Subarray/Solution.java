package problem53_Maximum_Subarray;

public class Solution {
	//对于每一个num，如果前面的和小于0，那么不需要加上previousSum，直接将previousSum置为num。
	 //否则previouseSum+num,并更新max。
	 public int maxSubArray(int[] nums) {
		 int max=Integer.MIN_VALUE,previousSum=0;
		 for(int num:nums){
			 if(previousSum<0) previousSum=num;
			 else previousSum+=num;
			 max=Math.max(previousSum, max);
		 }
		 return max;
	 }
}
