package problem112_Path_Sum;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class Solution {
	public boolean hasPathSum(TreeNode root, int sum) {
		if(root==null)
			return false;
		if(root.left==null&&root.right==null&&sum==root.val)
			return true;
		return hasPathSum(root.left,sum-root.val)||hasPathSum(root.right,sum-root.val);
    }
}
