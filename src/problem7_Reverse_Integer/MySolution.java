package problem7_Reverse_Integer;

public class MySolution {
	public int reverse(int x) {
		String s=x+"";
		int i=x<0? 1:0;
		char[] digits=s.toCharArray();
		int bound=x<0?digits.length/2+1:digits.length/2;
		while(i<bound){
			if(x<0){
				swap(digits,i,digits.length-i);
			}else{
				swap(digits,i,digits.length-1-i);
			}
			i++;
		}
		try{
			return Integer.parseInt(new String(digits));
		}catch(NumberFormatException e){
			return 0;
		}
	}
	
	private void swap(char[] input,int i,int j){
		char tmp=input[i];
		input[i]=input[j];
		input[j]=tmp;
	}
	
	public static void main(String[] args){
		MySolution s=new MySolution();
		System.out.println(s.reverse(10));
	}
}
