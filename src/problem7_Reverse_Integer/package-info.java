/**
 * Reverse digits of an integer.
 * Example1: x = 123, return 321
 * Example2: x = -123, return -321
 */
/**
 * @author Ivan
 *
 */
package problem7_Reverse_Integer;