package problem7_Reverse_Integer;

public class Solution {
	public int reverse(int x) {
		int result=0;boolean positive=x>0;
		while(x!=0){
			if(positive){
				if(result>(Integer.MAX_VALUE-x%10)/10){
					return 0;
				}
			}else{
				if(result<(Integer.MIN_VALUE-x%10)/10){
					return 0;
				}
			}
			result=result*10+x%10;
			x=x/10;
		}
		return result;
	}
	public static void main(String[] args){
		System.out.println(new Solution().reverse(-231));
	}
}
