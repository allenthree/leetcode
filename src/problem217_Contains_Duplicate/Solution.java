package problem217_Contains_Duplicate;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	 public boolean containsDuplicate(int[] nums) {
		 Map<Integer,Integer>  map=new HashMap<>();
		 for(int n:nums){
			 if(!map.containsKey(n)){
				 map.put(n, n);
			 }else{
				 return true;
			 }
		 }
		 return false;
	 }
}
