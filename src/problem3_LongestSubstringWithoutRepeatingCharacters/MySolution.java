package problem3_LongestSubstringWithoutRepeatingCharacters;

import java.util.HashMap;
import java.util.Map;

public class MySolution {
	public int lengthOfLongestSubstring(String s) {
		int max = 0;
		for (int i = 0; i < s.length(); i++) {
			if (max >= s.length() - i) {
				return max;
			} else {
				int currentLongestLength = lengthOfLongestSubstring(i, s);
				max = currentLongestLength > max ? currentLongestLength : max;
			}
		}
		return max;
	}

	private int lengthOfLongestSubstring(int start, String s) {
		Map<Character, Integer> map = new HashMap<>();
		for (int i = start; i < s.length(); i++) {
			if (map.containsKey(s.charAt(i))) {
				return map.size();
			}
			map.put(s.charAt(i), i);
		}
		return map.size();
	}
	
	public static void main(String[] args){
		MySolution solution=new MySolution();
		System.out.println(solution.lengthOfLongestSubstring(""));
		System.out.println(solution.lengthOfLongestSubstring("bbbbb"));
		System.out.println(solution.lengthOfLongestSubstring("pwwkew"));
	}

}
