package problem119_Pascals_Triangle_II;

import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<Integer> getRow(int rowIndex) {
		Integer[] array=new Integer[rowIndex+1];
		array[0]=1;
		for(int i=0;i<=rowIndex;i++){
			for(int j=i;j>0;j--){
				if(j==i)
					array[j]=array[j-1];
				else
					array[j]=array[j]+array[j-1];
			}
		}
		return Arrays.asList(array);
	}
}
