package problem203_Remove_Linked_List_Elements;

import problem82_Remove_Duplicates_from_Sorted_List2.ListNode;

public class Solution {
	 public ListNode removeElements(ListNode head, int val) {
		 ListNode dummy=new ListNode(-1);
		 dummy.next=head;
		 ListNode p=dummy;
		 while(p!=null){
			 if(p.next!=null&&p.next.val==val){
				 p.next=p.next.next;
			 }else{
				 p=p.next;
			 }
		 }
		 return dummy.next;
	 }
}
