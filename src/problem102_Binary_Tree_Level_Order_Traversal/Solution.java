package problem102_Binary_Tree_Level_Order_Traversal;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import problem95_UniqueBinarySearchTreesII.MySolution.TreeNode;

public class Solution {
	public List<List<Integer>> levelOrder(TreeNode root) {
		Queue<TreeNode> queue=new LinkedList<>();
		queue.add(root);	
		List<List<Integer>> result=new LinkedList<>();
		while(!queue.isEmpty()){
			int size=queue.size();
			List<Integer> currLevel=new LinkedList<>();
			while(size-->0){
				TreeNode top=queue.poll();
				if(top!=null){
					if(top.left!=null)
						queue.add(top.left);
					if(top.right!=null)
						queue.add(top.right);
					currLevel.add(top.val);
				}
			}
			if(currLevel.size()>0){
				result.add(currLevel);
			}
		}
		return result;
    }
}
