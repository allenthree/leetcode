package problem103_Binary_Tree_Zigzag_Level_Order_Traversal;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import problem95_UniqueBinarySearchTreesII.MySolution.TreeNode;

public class MySolution {
	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		Queue<TreeNode> queue=new LinkedList<>();
		queue.add(root);	
		List<List<Integer>> result=new LinkedList<>();
		int flag=0;
		while(!queue.isEmpty()){
			int size=queue.size();flag++;
			List<Integer> currLevel=new LinkedList<>();
			while(size-->0){
				TreeNode top=queue.poll();
				if(top!=null){
					if(top.left!=null)
						queue.add(top.left);
					if(top.right!=null)
						queue.add(top.right);
					if(flag%2==1)
						currLevel.add(top.val);
					else{
						currLevel.add(0,top.val);
					}
				}
			}
			if(currLevel.size()>0){
				result.add(currLevel);
			}
		}
		return result;
    }
}
