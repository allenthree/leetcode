package problem74_Search_a_2D_Matrix;

public class Solution {
	public boolean searchMatrix(int[][] matrix, int target) {
		int m=matrix.length,n=matrix[0].length,left=0,right=m*n-1;
		while(left<=right){
			int mid=left+right;
			if(matrix[mid/n][mid%n]==target)
				return true;
			else if(matrix[mid/n][mid%n]>target)
				right=mid-1;
			else
				left=mid+1;
		}
		return false;
    }
}
