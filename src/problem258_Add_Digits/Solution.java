package problem258_Add_Digits;

public class Solution {
	 public int addDigits(int num) {
		 //http://www.jianshu.com/p/6360ee10572f
		 return num==0?0:(num%9==0?9:(num%9));
	 }
}
