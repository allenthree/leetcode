package problem274_H_Index;

public class Solution {
	 public int hIndex(int[] citations) {
		 int n=citations.length;
		 int[] count=new int[n+1];
		 for(int c:citations){
			 if(c>=n)
				 count[n]++;
			 else
				 count[c]++;
		 }
		 for(int i=n;i>=0;i--){
			 if(count[i]>=i)
				 return i;
			 count[i-1]+=count[i];
		 }
		 return 0;
	 }
}
