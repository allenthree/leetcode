package problem108_Convert_Sorted_Array_to_Binary_Search_Tree;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) { val = x; }
    
    public String toString(){
  	  return "["+val+":left:"+(left==null?"null":left.toString())+",right:"+(right==null?"null":right.toString())+"]";
    }
}
