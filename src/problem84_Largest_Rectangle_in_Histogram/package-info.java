/**
 * Given n non-negative integers representing the histogram's bar height where the width of each bar
 * is 1, find the area of largest rectangle in the histogram.
 */
/**
 * @author Ivan
 *
 */
package problem84_Largest_Rectangle_in_Histogram;