package problem84_Largest_Rectangle_in_Histogram;

import java.util.Arrays;
import java.util.Stack;
/**
 * http://www.cnblogs.com/ganganloveu/p/4148303.html
 * 
 * 如果已知height数组是升序的，应该怎么做？
 * 比如1,2,5,7,8那么就是(1*5) vs. (2*4) vs. (5*3) vs. (7*2) vs. (8*1)
 * 
 * 因此使用栈维护一个升序序列
 * @author john
 *
 */
public class MySolution {
	public int largestRectangleArea(int[] height) {
		Stack<Integer> stack=new Stack<Integer>();
		height=Arrays.copyOf(height, height.length+1);	//make height[length]=0;
		int i=0,maxArea=0;
		while(i<height.length){
			if(stack.isEmpty()||height[i]>=height[stack.peek()]){
				stack.push(i++);
			}else{
				int tmp=stack.pop();
				maxArea=Math.max(maxArea,height[tmp]*(stack.isEmpty()?i:(i-1)-stack.peek()));
			}
		}
		return maxArea;
	}
	
	public static void main(String[] args){
		System.out.println(new MySolution().largestRectangleArea(new int[]{1,1,1}));
	}
}
