/**
 * Given an array of integers, every element appears twice except for one. Find that single one.
 */
/**
 * @author Ivan
 *
 */
package problem136_Single_Number;