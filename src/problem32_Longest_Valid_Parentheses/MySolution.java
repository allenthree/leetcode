package problem32_Longest_Valid_Parentheses;

import java.util.Arrays;
import java.util.Stack;

public class MySolution {
	public int longestValidParentheses(String s) {
		Stack<Integer> stack=new Stack<>();
		int max=0;
		char[] chars=s.toCharArray();
		boolean[] isParenthesesValid=new boolean[chars.length];
		for(int i=0;i<chars.length;i++){
			if(chars[i]=='('){
				stack.push(i);
			}else{
				if(!stack.isEmpty()){
					isParenthesesValid[stack.pop()]=true;
					isParenthesesValid[i]=true;
				}
			}
		}
		int tmp=0;
		for(int i=0;i<isParenthesesValid.length;i++){
			if(isParenthesesValid[i]){
				tmp++;
				max=Math.max(max, tmp);
			}else{
				tmp=0;
			}
		}
		return max;
	}
	
	public static void main(String[] args){
		System.out.println(new MySolution().longestValidParentheses("()"));
		Arrays.binarySearch(new int[]{},1);
	}
}
