package problem73_Set_Matrix_Zeroes;


public class Solution {
	public void setZeroes(int[][] matrix) {
		int firstRowHasZero=-1, m=matrix.length,n=matrix[0].length;
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				if(matrix[i][j]==0){
					if(i==0){
						firstRowHasZero=1;
					}else{
						matrix[i][0]=0;
					}
					matrix[0][j]=0;
				}
			}
		}
		for(int i=1;i<m;i++){
			if(matrix[i][0]==0)//set row i zero
				for(int j=0;j<n;j++) matrix[i][j]=0;
		}
		for(int i=0;i<n;i++){
			if(matrix[0][i]==0) //set column i zero
				for(int j=0;j<m;j++) matrix[j][i]=0;
		}
		if(firstRowHasZero==1) 
			for(int j=0;j<n;j++) matrix[0][j]=0;
    }
	
	public static void main(String[] args){
		int[][] matrix=new int[][]{{0,1,1},{1,2,1},{3,3,1}};
		new Solution().setZeroes(matrix);
		int m=matrix.length,n=matrix[0].length;
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}
	}
}
