package problem205_Isomorphic_Strings;

public class Solution {
	public static void main(String[] args){
		System.out.println(new Solution().isIsomorphic("ab","aa"));
	}
	
	public boolean isIsomorphic(String s, String t) {
        char[] arr1=s.toCharArray();
        char[] arr2=t.toCharArray();
        int[] map=new int[256];
        int[] map2=new int[256];
        for(int i=0;i<arr1.length;i++){
        	if(map[arr1[i]]!=0){
        		if(map[arr1[i]]!=arr2[i])
        			return false;
        	}else{
        		if(map2[arr2[i]]!=0){
        			if(arr1[i]!=map2[arr2[i]])
        				return false;
        		}
        		map[arr1[i]]=arr2[i];
        		map2[arr2[i]]=arr1[i];
        	}
        }
        return true;
    }
}
