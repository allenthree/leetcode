package totp;


/**
 * This class helps to generate QR code according to google-authenticator Key
 * Uri Format API.
 * 
 * @author Ivan,Zhang
 * @see <a href=
 *      "https://github.com/google/google-authenticator/wiki/Key-Uri-Format"/>
 *
 */
public class SecretKeyQRCodeGenerator {
	private static final String keyURIFormat="otpauth://totp/"+"%s"+":"+"%s"+"?secret=%s&issuer=%s";

	public static String getTOTPQRCodeURL(String issuer,String label,String secret){
		if (issuer != null) {
            if (issuer.contains(":")||issuer.contains(" ")) {
                throw new IllegalArgumentException("Issuer cannot contain the \':\' and blank character.");
            }
        }
		return String.format(keyURIFormat, issuer, label,secret,issuer);
	}
}
