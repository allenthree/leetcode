package totp;

import java.util.Random;

/**
 * This class accepts an alphabet to generate a random string with specific length.
 * 
 * @author Ivan,Zhang
 */
public class SecretKeyGenerator {
	private final String alphabet;

	public SecretKeyGenerator(String alphabet) {
		super();
		this.alphabet = alphabet;
	}

	public  String run(int secretKeyLength){
		Random r=new Random();
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<secretKeyLength;i++){
			sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
		}
		return sb.toString();
	}
}
