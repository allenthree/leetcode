package totp;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Ivan,Zhang
 * @see <a href="https://tools.ietf.org/html/rfc6238" />
 *
 */
public class TOTPConfig {
	/**
	 * the time step in milliseconds and the default value is 30s.
	 */
	public static final long timeStepSize = TimeUnit.SECONDS.toMillis(30);

	/**
	 * Unix time to start counting time steps.
	 */
	public static final long timeStart = 0L;

	/**
	 * The hash algorithm in HMAC(keyed-hash message authentication code). The
	 * default value is HmacSHA1. Optional value:HmacSHA256,HmacSHA512
	 */
	public static final String secretKeyAlgorithm = "HmacSHA1";

	/**
	 * The receiving time at the validation system and the actual OTP generation
	 * may not fall within the same time-step window that produced the same OTP.
	 * The validation system should compare OTPs not only with the receiving
	 * timestamp but also the past timestamps that are within the transmission
	 * delay.If you set allowedPassedValidationWindows=1, this would mean the
	 * validator could perform a validation against the current time and then
	 * a further validations for the passed time-step (for a total of 2
	 * validations).
	 */
	public static final int allowedPastValidationWindows = 10; //set as 5 minutes.
	
	
	public static final int allowedFutureValidationWindows = 2;

	/**
	 * The length of the time-based one-time password.
	 */
	public static final int TOTPLength = 6;

}
