package problem232_Implement_Queue_using_Stacks;

import java.util.Stack;

/**
 * inStack用于插入数据，outStack用于读取数据
 * 
 * @author I321035
 *
 */
public class MyQueue {
	private Stack<Integer> inStack=new Stack<>();
	private Stack<Integer> outStack=new Stack<>();
	
    // Push element x to the back of queue.
    public void push(int x) {
    	inStack.push(x);
    }

    // Removes the element from in front of queue.
    public void pop() {
    	peek();
    	outStack.pop();
    }

    // Get the front element.
    public int peek() {
    	if(outStack.empty()){
    		while(!inStack.empty())
    			outStack.push(inStack.pop());
    	}
    	return outStack.peek();
    }

    // Return whether the queue is empty.
    public boolean empty() {
    	return inStack.empty()&&outStack.empty();
    }
}
