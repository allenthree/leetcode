package problem151_Reverse_Words_in_a_String;

import java.util.Arrays;

public class Solution {
	 public String reverseWords(String s) {
		 String[] tokens=s.trim().split("\\s+");
		 StringBuilder sb=new StringBuilder();
		 for(int i=tokens.length-1;i>=0;i--){
			 sb.append(tokens[i]);
			 if(i!=0)
				 sb.append(" ");
		 }
		 return sb.toString();
	 }
	 
	 public static void main(String[] args){
		 String s="abc'd   efs";
		 System.out.println(Arrays.toString(s.split("\\s+")));
		 System.out.println(new Solution().reverseWords(" 1 "));
	 }
}
