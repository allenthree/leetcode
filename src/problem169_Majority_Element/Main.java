package problem169_Majority_Element;

public class Main {
	public int majorityElement(int[] nums) {
        int count=0,candidate=0;
        for(int n:nums){
        	if(count==0){
        		candidate=n;
        		count++;
        	}else{
        		if(candidate==n)
        			count++;
        		else
        			count--;
        	}
        }
        return candidate;
    }
}
