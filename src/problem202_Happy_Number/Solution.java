package problem202_Happy_Number;

import java.util.HashSet;
import java.util.Set;

public class Solution {
	 public boolean isHappy(int n) {
		 Set<Integer> appeared=new HashSet<>();
		 while(!appeared.contains(n)){
			 appeared.add(n);
			 int newN=0;
			 while(n!=0){
				 newN+=(n%10)*(n%10);
				 n/=10;
			 }
			 n=newN;
			 if(n==1)
				 return true;
		 }
		 return false;
	 }
}
