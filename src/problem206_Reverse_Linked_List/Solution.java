package problem206_Reverse_Linked_List;

import problem82_Remove_Duplicates_from_Sorted_List2.ListNode;

public class Solution {
	 public ListNode reverseList(ListNode head) {
	        ListNode dummy=new ListNode(-1);
	        while(head!=null){
	        	ListNode tmp=head.next;
	        	head.next=dummy.next;
	        	dummy.next=head;
	        	head=tmp;
	        }
	        return dummy.next;
	 }
}
