package problem165_Compare_Version_Numbers;

public class Solution {
	public static void main(String[] args){
		System.out.println(new Solution().compareVersion("1.10.1","1.10" ));
	}
	
	 public int compareVersion(String version1, String version2) {
		 if(version1.equals(version2))
			 return 0;
		 String[] v1=version1.split("\\.");
		 String[] v2=version2.split("\\.");
		 int i=0;
		 for(i=0;i<v1.length||i<v2.length;i++){
			 //长度较短的string补零
			 char[] s1=i<v1.length?v1[i].toCharArray():"".toCharArray(),s2=i<v2.length?v2[i].toCharArray():"".toCharArray();
			 int sum1=0,sum2=0;
			 for(int j=0;j<s1.length;j++){
				 sum1=sum1*10+s1[j]-'0';
			 }
			 for(int j=0;j<s2.length;j++){
				 sum2=sum2*10+s2[j]-'0';
			 }
			 if(sum1!=sum2){
				 return Integer.compare(sum1, sum2);
			 }
		 }
		 return 0;
	 }
	 
}
