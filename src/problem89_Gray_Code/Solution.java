package problem89_Gray_Code;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @see <a href="http://www.matrix67.com/blog/archives/266" />
 *
 */
public class Solution {
	public List<Integer> grayCode(int n) {
		List<Integer> result=new LinkedList<>();
		int j=1<<n;
		for(int i=0;i<j;i++){
			result.add(i^(i>>1));
		}
		return result;
    }
}
