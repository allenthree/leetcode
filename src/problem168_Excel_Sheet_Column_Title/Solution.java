package problem168_Excel_Sheet_Column_Title;

public class Solution {
	public static void main(String[] args){
		System.out.println(new Solution().convertToTitle(29000));
	}
	
	public String convertToTitle(int n) {
		String result="";
		while(n!=0){
			result=((char)(((n-1)%26)+'A'))+result;
			n=(n-1)/26;
		}
		return result;
    }
}
