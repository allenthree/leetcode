package problem45_Jump_Game_II;

public class Solution {
	public int jump(int[] nums) {
		 int farest=0,steps=0,end=0;
		 for(int i=0;i<nums.length-1;i++){
			 farest=Math.max(farest, nums[i]+i);
			 if(i==end){
				 steps++;
				 end=farest;
			 }
		 }
		 return steps;
	 }
	
	public static void main(String[] args){
		System.out.println(new Solution().jump(new int[]{2,1,3}));
	}
}
