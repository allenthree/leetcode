package problem191_Number_of1Bits;

public class Solution {
	 public int hammingWeight(int n) {
		 int sum=0;
		 while(n!=0){
			 sum++;
			 n&=(n-1);//将最低位的1置为0
		 }
		 return sum;
	 }
}
