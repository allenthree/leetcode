package problem167_Two_Sum_II;

public class Solution {
	public int[] twoSum(int[] numbers, int target) {
		int left=0,right=numbers.length-1;
		while(left<right){
			int mid=(left+right)>>1;
			if(numbers[left]+numbers[right]==target)
				return new int[]{left+1,right+1};
			else if(numbers[left]+numbers[right]<target)
				left=(numbers[mid]+numbers[right]<=target)?mid:left+1;
			else
				right=(numbers[mid]+numbers[left]>=target)?mid:right-1;
		}
		return new int[]{};
	}
}
