package problem67_Add_Binary;

public class MySolution {
	public String addBinary(String a, String b) {
		char[] charAarray=a.toCharArray();
		char[] charBarray=b.toCharArray();
		String result="";
		int carry=0,i=charAarray.length-1,j=charBarray.length-1;
		while(carry==1||i>=0||j>=0){
			int sum=carry+(i>=0?charAarray[i--]-'0':0)+(j>=0?charBarray[j--]-'0':0);
			carry=sum/2;
			result=sum%2+result;
		}
		return result;
    }
	
	public static void main(String[] args){
		System.out.println(new MySolution().addBinary("100", "1"));
	}
}
