/**
 * Given a string of numbers and operators, 
 * return all possible results from computing all the different possible ways to
 * group numbers and operators. The valid operators are +, - and *.
 * 
 * For example,input:2-1-1
 * ((2-1)-1) = 0
 * (2-(1-1)) = 2
 * 
 * output:
 * [0,2]
 * 
 */
/**
 * @author I321035
 *
 */
package problem241_Different_Ways_to_Add_Parentheses;