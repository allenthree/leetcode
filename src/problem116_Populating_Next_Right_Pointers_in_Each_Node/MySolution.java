package problem116_Populating_Next_Right_Pointers_in_Each_Node;


public class MySolution {
	public class TreeLinkNode {
		      int val;
		      TreeLinkNode left, right, next;
		      TreeLinkNode(int x) { val = x; }
	}
	
	public void connect(TreeLinkNode root) {
		while(root!=null){
			TreeLinkNode cur=root;
			while(cur!=null){
				if(cur.left!=null)
					cur.left.next=cur.right;
				if(cur.right!=null)
					cur.right.next=(cur.next==null?null:cur.next.left);
				cur=cur.next;
			}
			root=root.left;
		}
    }
}	
