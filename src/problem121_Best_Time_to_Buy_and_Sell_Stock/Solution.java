package problem121_Best_Time_to_Buy_and_Sell_Stock;

public class Solution {
	public int maxProfit(int[] prices) {
        int minPrice=Integer.MAX_VALUE,max=0;
        for(int p:prices){
        	if(p<minPrice)
        		minPrice=p;
        	else
        		max=Math.max(max, p-minPrice);
        }
        return max;
    }
}
