package problem19_RemoveNthNodeFromEndofList;

public class Solution {

	public ListNode removeNthFromEnd(ListNode head, int n) {
		ListNode dummy=new ListNode(0);
		dummy.next=head;
		ListNode first=dummy,second=dummy;
		for(int i=0;i<=n;i++){
			first=first.next;
		}
		while(first!=null){
			first=first.next;
			second=second.next;
		}
		second.next=second.next.next;
		return dummy.next;
	}

	public class ListNode {
		public int val;
		public ListNode next;

		public ListNode(int x) {
			val = x;
		}
	}
}
