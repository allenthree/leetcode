package problem242_Valid_Anagram;

public class Solution {
	 public boolean isAnagram(String s, String t) {
		 if(s.length()!=t.length())
			 return false;
		 int[] arr=new int[256];
		 char[] chars=s.toCharArray();
		 for(char c:chars){
			 arr[c]++;
		 }
		 char[] chart=t.toCharArray();
		 for(char c:chart){
			 if(arr[c]==0)
				 return false;
			 arr[c]--;
		 }
		 return true;
	 }
}
