package problem104_Maximum_Depth_of_Binary_Tree;

import problem95_UniqueBinarySearchTreesII.MySolution.TreeNode;

public class Solution {
	public int maxDepth(TreeNode root) {
		if(root==null)
			return 0;
		return 1+Math.max(maxDepth(root.left),maxDepth(root.right));
    }
}
