/**
 * Given a binary tree, find its maximum depth.
 * The maximum depth is the number of nodes along the longest path from the root node down to the farthest 
 * leaf node.
 */
/**
 * @author Ivan
 *
 */
package problem104_Maximum_Depth_of_Binary_Tree;