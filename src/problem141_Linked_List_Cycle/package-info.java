/**
 * Given a linked list, determine if it has a cycle in it.
 * Follow up:Can you solve it without using extra space?
 */
/**
 * @author Ivan
 *
 */
package problem141_Linked_List_Cycle;