package problem141_Linked_List_Cycle;

import problem19_RemoveNthNodeFromEndofList.Solution.ListNode;

public class Solution {
	 public boolean hasCycle(ListNode head) {
	     ListNode fast=head,slow=head;
	     while(fast!=null&&fast.next!=null){
	    	 fast=fast.next.next;
	    	 slow=slow.next;
	    	 if(fast==slow)
	    		 return true;
	     }
	     return false;
	 }
}
