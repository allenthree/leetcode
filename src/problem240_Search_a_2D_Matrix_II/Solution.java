package problem240_Search_a_2D_Matrix_II;

public class Solution {
	 public boolean searchMatrix(int[][] matrix, int target) {
		 int m=matrix.length,n=matrix[0].length;
		 int left=m-1,right=0;
		 while(left>=0&&right<n){
			 int curr=matrix[left][right];
			 if(curr==target)
				 return true;
			 else if(curr<target)
				 right++;
			 else
				 left--;
		 }
		 return false;
	 }
}
