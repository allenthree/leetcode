package a_primeFactorization;

public class PrimeFactorization {
	public static void main(String[] args){
		System.out.println(PrimeFactorization.factorize(1028));
	}
	
	public static String factorize(int n){
		int i=2;
		StringBuilder sb=new StringBuilder(String.valueOf(n)+"=");
		while(i<=n){
			if(n==i){
				sb.append(i);
				break;
			}else if(n%i==0){
				sb.append(i+"*");
				n/=i;
				i=2;
			}else{
				i++;
			}
		}
		return sb.toString();
	}
}
