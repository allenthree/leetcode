package problem75_Sort_Colors;

public class Solution {
	public void sortColors(int[] nums) {
		int red=0,blue=nums.length-1,i=0;
		while(i<=blue){
			if(nums[i]==0) swap(red++,i++,nums);
			else if(nums[i]==2) swap(blue--,i,nums);
			else
				i++;
		}
    }
	
	private void swap(int i,int j,int[] nums){
		int tmp=nums[i];
		nums[i]=nums[j];
		nums[j]=tmp;
	}
}
