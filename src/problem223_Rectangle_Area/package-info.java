/**
 * Find the total area covered by two rectilinear rectangles in a 2D plane.
 */
/**
 * @author I321035
 *
 */
package problem223_Rectangle_Area;