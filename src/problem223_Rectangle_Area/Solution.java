package problem223_Rectangle_Area;

public class Solution {
	public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
		int a1=(C-A)*(D-B),a2=(G-E)*(H-F);
		if(E>=C||B>=H||A>=G||F>=D)
			return a1+a2;
		int intersect=(Math.min(H, D)-Math.max(F, B))*(Math.min(G, C)-Math.max(A, E));
		return a1+a2-intersect;
    }
}
