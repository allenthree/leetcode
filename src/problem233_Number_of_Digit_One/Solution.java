package problem233_Number_of_Digit_One;

/**
 * 假设n=abcde五位数字的时候，我们分析百位c，有三种情况：
 * 1）c == 0的时候，比如12013，此时百位出现1的是：00 100 ~ 00 199， 01 100~01 199，……，11 100~ 11 199，共1200个，
 * 显然这个有高位数字决定，并且受当前位数影响； 个数就是 ab*100
 * 
 * 2）c == 1的时候，比如12113，此时百位出现1的肯定包括c=0的情况，另外还需要考虑低位的情况即：00100 ~ 00113共14个; 个数等于ab*100+ de + 1
 * 
 * 3）c >= 2的时候，比如12213，此时百位出现1的是：00 100 ~ 00 199， 01 100~01 199，……，11 100~ 11 199，12 100 ~ 12 199，共1300个，
 * 这个有高位数字决定，其实是加一，并且乘以当前位数; 个数就是 (ab+1)*100
 * 
 * 总结起来，对于一个 n = abcde 来说，百位出现1的个数计算方法为 ：
 * 
 * if(c==0) ans = ab*100;
 * if(c==1) ans = ab*100+cd+1
 * if(c>1) ans = (ab+1)*100
 *
 */
public class Solution {
	public int countDigitOne(int n) {
		int left=n,level=1,result=0;
		while(left>0){
			int curr=left%10,right=n%level;
			left/=10;
			if(curr==0)
				result+=left*level;
			else if(curr==1)
				result+=left*level+right+1;
			else
				result+=(left+1)*level;
			level*=10;
		}
		return result;
    }
}
