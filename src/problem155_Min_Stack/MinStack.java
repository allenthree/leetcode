package problem155_Min_Stack;

import java.util.Stack;

public class MinStack {
	private Stack<Integer> elements;
	private Stack<Integer> minElements;

    /** initialize your data structure here. */
    public MinStack() {
        elements=new Stack<>();
        minElements=new Stack<>();
    }
    
    public void push(int x) {
        elements.push(x);
        if(minElements.isEmpty()||minElements.peek()>=x){
        	minElements.push(x);
        }
    }
    
    public void pop() {
        int e=elements.pop();
        if(e==minElements.peek())
        	minElements.pop();
    }
    
    public int top() {
        return elements.peek();
    }
    
    public int getMin() {
        return minElements.peek();
    }
}