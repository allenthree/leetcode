package problem278_First_Bad_Version;

public class Solution {
	 public int firstBadVersion(int n) {
		 int left=1,right=n;
		 while(left<=right){
			 //left+high有可能数据溢出
			// int mid=(left+right)/2;
			 int mid=left+(right-left)/2;
			 if(isBadVersion(mid)){
				 right=mid-1;
			 }else{
				 left=mid+1;
			 }
		 }
		 return left;
	 }
	 
	 boolean isBadVersion(int version){
		return false;
	 }
}
