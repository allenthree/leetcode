package problem134_Gas_Station;

public class Solution {
	public int canCompleteCircuit(int[] gas, int[] cost) {
		int begin=0,i=0,left=0,total=0;
		while(i<gas.length){
			left+=gas[i]-cost[i];
			total+=gas[i]-cost[i];
			if(left<0){
				left=0;
				begin=i+1;
			}
			i++;
		}
		return total>=0?begin:-1;
    }
}
