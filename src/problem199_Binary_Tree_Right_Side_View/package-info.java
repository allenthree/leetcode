/**
 * Given a binary tree, imagine yourself standing on the right side of it, return the values of the nodes
 *  you can see ordered from top to bottom.For example:
 *  Given the following binary tree,
   1            <---
 /   \
2     3         <---
 \     \
  5     4       <---
You should return [1, 3, 4].
 */
/**
 * @author I321035
 *
 */
package problem199_Binary_Tree_Right_Side_View;