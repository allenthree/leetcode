package problem199_Binary_Tree_Right_Side_View;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import problem105_Construct_Binary_Tree_from_Preorder_and_Inorder_Traversal.MySolution.TreeNode;

public class Solution {
	 public List<Integer> rightSideView(TreeNode root) {
		 List<Integer> result=new LinkedList<>();
		 if(root==null)
			 return result;
		 Queue<TreeNode> queue=new LinkedList<>();
		 queue.add(root);
		 while(!queue.isEmpty()){
			 int size=queue.size();
			 while(size-->0){
				 TreeNode curr=queue.poll();
				 if(size==0)
					 result.add(curr.val);
				 if(curr.left!=null)
					 queue.add(curr.left);
				 if(curr.right!=null)
					 queue.add(curr.right);
			 }
		 }
		 return result;
	 }
}
