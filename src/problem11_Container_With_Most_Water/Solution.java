package problem11_Container_With_Most_Water;

public class Solution {
	public int maxArea(int[] height) {
		if(height.length<2) return 0;
		int i=0,j=height.length-1,maxArea=0;
		while(i<j){
			maxArea=Math.max(maxArea, Math.min(height[i], height[j])*(j-i));
			if(height[i]<height[j]){
				int tmp=i;
				while(height[i]<=height[tmp]&&i<j){
					i++;
				}
			}
			else{
				int tmp=j;
				while(height[j]<=height[tmp]&&i<j){
					j--;
				}
			}
		}
		return maxArea;
	}
	
	public static void main(String[] args){
		System.out.println(new Solution().maxArea(new int[]{2,1,1,4,2}));
	}
}
