/**
 * Divide two integers without using multiplication, division and mod operator.
 * If it is overflow, return MAX_INT.
 */
/**
 * @author Ivan
 *
 */
package problem29_DivideTwoIntegers;