package problem29_DivideTwoIntegers;

public class Solution {
	public int divide(int dividend, int divisor) {
		if(divisor==0) return Integer.MAX_VALUE;
		if(dividend==Integer.MIN_VALUE&&divisor==-1) return Integer.MAX_VALUE;
		boolean isNeg=dividend>0&&divisor<0||dividend<0&&divisor>0;
		long dividend_long= Math.abs((long) dividend);
		long divisor_long=Math.abs((long) divisor);
		int pow=0;
		for(pow=0;divisor_long<<(pow+1)<=dividend_long;pow++);
		int res=0;
		while(dividend_long>=divisor_long){
			if(dividend_long>=divisor_long<<pow){
				dividend_long-=divisor_long<<pow;
				res+=1<<pow;
			}
			pow--;
		}
		return isNeg?0-res:res;
    }
	
	public static void main(String[] args){
		System.out.println(new Solution().divide(Integer.MIN_VALUE, -1));
	}
}
