package problem137_Single_Number_II;

public class Solution {
	 public int singleNumber(int[] nums) {
		 int result=0;
		 for(int i=0;i<32;i++){
			 int sum=0;
			 for(int n:nums){
				sum+=(n>>i)&1;
			 }
			 sum%=3;
			 result|=(sum<<i);
		 }
		 return result;
	 }
	 
	 public static void main(String[] args){
		 System.out.println(new Solution().singleNumber(new int[]{3,3,3,2,1,1,1}));
	 }
}
