package problem39_Combination_Sum;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Solution {
	private List<List<Integer>> result = new LinkedList<>();
	private List<Integer> current=new LinkedList<>();
	
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		Arrays.sort(candidates);
		backtracking(candidates,target,0);
		return result;
	}

	private void backtracking(int[] candidates,int rest_target,int lastUsed){
		  if(rest_target==0)
			  result.add(new LinkedList<>(current));
		  for(int i=lastUsed;i<candidates.length&&candidates[i]<=rest_target;i++){
			  current.add(candidates[i]);
			  backtracking(candidates,rest_target-candidates[i],i);
			  current.remove(current.size()-1);
		  }
	}
	
	public static void main(String[] args){
		System.out.println(new Solution().combinationSum(new int[]{2,3,6,7},7));
	}
}
