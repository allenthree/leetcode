/**
 * Invert a binary tree.

     4
   /   \
  2     7
 / \   / \
1   3 6   9
to
     4
   /   \
  7     2
 / \   / \
9   6 3   1
 */
/**
 * @author I321035
 *
 */
package problem226_Invert_Binary_Tree;