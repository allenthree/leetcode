package problem77_Combinations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MySolution {
	public List<List<Integer>> combine(int n, int k) {
		List<List<Integer>> result=new LinkedList<>();
		combine(n,k,new Integer[k],result);
		return result;
    }
	
	private void combine(int end,int r,Integer[] rs,List<List<Integer>> result){
		for(int i=end;i>=r;i--){
			rs[r-1]=i;
			if(r==1){
				result.add(new ArrayList<Integer>(Arrays.asList(rs)));
			}else{
				combine(i-1,r-1,rs,result);
			}
		}
	}
	
	public static void main(String[] args){
		System.out.println(new MySolution().combine(4, 2));
	}
}
