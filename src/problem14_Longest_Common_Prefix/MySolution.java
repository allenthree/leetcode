package problem14_Longest_Common_Prefix;

public class MySolution {
	public String longestCommonPrefix(String[] strs) {
		if (strs.length == 0)
			return "";
		if (strs.length == 1)
			return strs[0];
		char[] firstStr = strs[0].toCharArray();
		if (firstStr.length == 0)
			return "";
		int min = firstStr.length;
		for (int i = 1; i < strs.length; i++) {
			if (strs[i].equals(""))
				return "";
			int j = 0;
			while (j < Math.min(strs[i].length(), firstStr.length)) {
				if (strs[i].charAt(j) == firstStr[j])
					j++;
				else 
					break;
			}
			min = Math.min(j, min);
		}
		return strs[0].substring(0, min);
	}

	public static void main(String[] args) {
		new MySolution().longestCommonPrefix(new String[] { "c", "c" });
	}
}
