package problem228_Summary_Ranges;

import java.util.LinkedList;
import java.util.List;

public class Solution {
	 public List<String> summaryRanges(int[] nums) {
		 List<String> result=new LinkedList<>();
		 if(nums.length==0)
			 return result;
	     int start=nums[0],end=nums[0]; 
		 for(int i=1;i<nums.length;i++){
			 if(nums[i]-nums[i-1]==1)
				 end=nums[i];
			 else{
				 result.add(transferToString(start, end));
				 start=nums[i];	 
				 end=nums[i];	 
			 }
	     }
		 result.add(transferToString(start, end));
		 return result;
	 }
	 
	 private String transferToString(int start,int end){
		 String result="";
		 if(start==end)
			 result=String.valueOf(start);
		 else
			 result=start+"->"+end;
		 return result;
	 }
}
