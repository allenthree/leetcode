package problem275_H_Index_II;

public class Solution {
	 public int hIndex(int[] citations) {
		 int left=0,right=citations.length-1;
		 while(left<=right){
			int mid=(left+right)>>1;
		 	if(citations.length-mid<=citations[mid]){
		 		right=mid-1;
		 	}else{
		 		left=mid+1;
		 	}
		 }
		 return citations.length-left;
	 }
}
