package problem237_Delete_Node_in_a_Linked_List;

import problem19_RemoveNthNodeFromEndofList.Solution.ListNode;

public class Solution {
	public void deleteNode(ListNode node) {
		if(node==null)	return;
		node.val=node.next.val;
		node.next=node.next.next;
    }
}
