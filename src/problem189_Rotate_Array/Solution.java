package problem189_Rotate_Array;

import java.util.Arrays;

public class Solution {
	public static void main(String[] args){
		int[] a={1,2,3,4,5,6,7};
		new Solution().rotate(a, 6);
		System.out.println(Arrays.toString(a));
	}
	
	 public void rotate(int[] nums, int k) {
		 k=nums.length-k%nums.length;
		 reverse(nums,0,k-1);
		 reverse(nums,k,nums.length-1);
		 reverse(nums,0,nums.length-1);
	 }
	 
	 private void reverse(int[] nums,int left,int right){
		 while(left<right){
			 int tmp=nums[left];
			 nums[left]=nums[right];
			 nums[right]=tmp;
			 left++;
			 right--;
		 }
	 }
}
