package array_to_list;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
	public static void main(String[] args){
		int[] arr=new int[]{1,2,3};
		List<Integer> list1=Arrays.asList(1,2,4);
		System.out.println(list1);
		//int[] 数组则不能使用asList方法
		List<Integer> list2=IntStream.of(arr).boxed().collect(Collectors.toList());
		System.out.println(list2);
	}
}
