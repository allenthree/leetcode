package problem98_Validate_Binary_Search_Tree;

import problem95_UniqueBinarySearchTreesII.MySolution.TreeNode;

public class MySolution {
	TreeNode pre=null;
	public boolean isValidBST(TreeNode root) {
		if(root!=null){
			if(!isValidBST(root.left)) 
				return false;
			if(pre!=null&&pre.val>=root.val)
				return false;
			pre=root;
			return isValidBST(root.right);
		}
		return true;
	}
}
