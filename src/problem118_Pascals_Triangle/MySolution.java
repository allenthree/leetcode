package problem118_Pascals_Triangle;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MySolution {
	 public List<List<Integer>> generate(int numRows) {
         List<List<Integer>> result=new LinkedList<>();
         if(numRows==0)	return result;
         result.add(new LinkedList<>(Arrays.asList(1)));
		 for(int i=1;i<numRows;i++){
			 List<Integer> lastRow=result.get(i-1);
			 List<Integer> currentRow=new LinkedList<>();
			 currentRow.add(1);
        	 for(int j=1;j<i;j++){
        		 currentRow.add(lastRow.get(j)+lastRow.get(j-1));
        	 }
        	 currentRow.add(1);
        	 result.add(currentRow);
         }
		 return result;
     }
	 
	 public static void main(String[] args){
		 System.out.println(new MySolution().generate(3));
	 }
}
