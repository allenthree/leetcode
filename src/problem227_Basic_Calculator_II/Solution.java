package problem227_Basic_Calculator_II;

/**
 * 使用辅助变量d记录当前待参与运算的运算数，func记录上一个加减运算符，total记录表达式的值。
 * 若当前运算符为乘除法，则马上对d与下一个运算数执行乘除运算，赋值给d；
 * 若当前运算符为加减法，则对total与d执行func（加减）运算，赋值给total，并更新func；
 * 
 * @author I321035
 *
 */
public class Solution {
	 public int calculate(String s) {
		 int result=0;char preOperator='+';int preOperand=0;
		 char[] tokens=s.toCharArray();
		 for(int i=0;i<tokens.length;i++){
			 if(Character.isDigit(tokens[i])){
				 int currOperand=0;
				 while(i<tokens.length&&Character.isDigit(tokens[i]))
					 currOperand=currOperand*10+(tokens[i++]-'0');
				 if(preOperator=='+'||preOperator=='-'){
					 result+=preOperand;
					 preOperand=currOperand*(preOperator=='-'?-1:1);
				 }else if(preOperator=='*'){
					 preOperand*=currOperand;
				 }else if(preOperator=='/'){
					 preOperand/=currOperand;
				 }
			 }
			 if(i<tokens.length&&tokens[i]!=' '){
				 preOperator=tokens[i];
			 }
		 }
		 return result+preOperand;
	 }
	 
	 public static void main(String[] args){
		 System.out.println(new Solution().calculate("2+3+4"));
	 }
	 
}
