/**
 * Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 * Note: Do not modify the linked list.
 * Follow up:Can you solve it without using extra space?
 */
/**
 * @author Ivan
 *
 */
package problem142_Linked_List_Cycle_II;