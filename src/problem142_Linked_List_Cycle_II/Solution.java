package problem142_Linked_List_Cycle_II;

import problem82_Remove_Duplicates_from_Sorted_List2.ListNode;

public class Solution {
	public ListNode detectCycle(ListNode head) {
        ListNode slow=head,fast=head;
        while(fast!=null&&fast.next!=null){
        	fast=fast.next.next;
        	slow=slow.next;
        	if(fast==slow){
        		ListNode slow2=head;
        		while(slow2!=slow){
        			slow=slow.next;
        			slow2=slow2.next;
        		}
        		return slow2;
        	}
        }
        return null;
    }
}
