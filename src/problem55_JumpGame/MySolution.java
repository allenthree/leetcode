package problem55_JumpGame;

public class MySolution {
	public boolean canJump(int[] nums) {
		int end = 0, nextEnd = 0;
		for (int i = 0; i < nums.length; i++) {
			nextEnd = Math.max(nextEnd, nums[i] + i);
			if (nums[i] + i >= nums.length - 1)
				return true;
			if (i == end) {
				if (nextEnd <= end)
					return false;
				end = nextEnd;
			}
		}
		return false;
	}
}
