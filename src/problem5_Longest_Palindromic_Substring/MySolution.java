package problem5_Longest_Palindromic_Substring;

public class MySolution {
	public String longestPalindrome(String s) {
		char[] input=s.toCharArray();
		String max="";
		for(int i=0;i<input.length;i++){
			for(int j=input.length-1;j>=i;j--){
				if(isPalidromic(input,i,j)){
					max=max.length()>j-i+1?max:charToStr(input,i,j);
					break;
				}
			}
		}
		return max;
	}
	
	public boolean isPalidromic(char[] input,int start ,int end){
		for(int i=start;i<=(end+start)/2;i++){
			if(input[i]!=input[start+end-i]){
				return false;
			}
		}
		return true;
	}
	
	private String charToStr(char[] input,int start,int end){
		StringBuffer sb=new StringBuffer();
		for(int i=start;i<=end;i++){
			sb.append(input[i]);
		}
		return sb.toString();
	}
	
	public static void main(String[] args){
		MySolution s=new MySolution();
		System.out.println(s.longestPalindrome("aaaabbaaa"));
	}
}
