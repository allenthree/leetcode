/**
 * Given a string S, find the longest palindromic substring in S. You may assume that 
 * the maximum length of S is 1000, and there exists one unique longest palindromic substring.
 */
/**
 * @author Ivan
 *
 */
package problem5_Longest_Palindromic_Substring;