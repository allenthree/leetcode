package problem110_Balanced_Binary_Tree;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class Solution {
	public boolean isBalanced(TreeNode root) {
		return balancedTreeHeight(root)>=0;
    }
	
	private int balancedTreeHeight(TreeNode root){
		if(root==null)	return 0;
		int heighL=balancedTreeHeight(root.left);
		int heightR=balancedTreeHeight(root.right);
		if(heighL<0||heightR<0||Math.abs(heighL-heightR)>1)	return -1;
		return 1+Math.max(heighL, heightR);
	}
}
