package problem135_Candy;

public class Solution {
	public int candy(int[] ratings) {
		int sum=0;int[] canday=new int[ratings.length];
		canday[0]=1;
		for(int i=1;i<ratings.length;i++){
			canday[i]=1;
			if(ratings[i]>ratings[i-1]){
				canday[i]=canday[i-1]+1;
			}
		}
		for(int i=ratings.length-2;i>=0;i--){
			if(ratings[i]>ratings[i+1]&&canday[i]<=canday[i+1]){
				canday[i]=canday[i+1]+1;
			}
			sum+=canday[i];
		}
		return sum+=canday[canday.length-1];
	}
}
