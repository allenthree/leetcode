package problem78_Subsets;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MySolutionUsingdfs {
	public List<List<Integer>> subsets(int[] nums) {
		List<List<Integer>> result=new LinkedList<>();
		List<Integer> temp = new ArrayList<Integer>();  
		dfs(result,temp,0,nums);
		return result;
	}
	
	private void dfs(List<List<Integer>> result,List<Integer> tmp,int start,int[] nums){
		result.add(new ArrayList<>(tmp));
		for(int i=start;i<nums.length;i++){
			tmp.add(nums[i]);
			dfs(result,tmp,i+1,nums);
			tmp.remove(tmp.size()-1);
		}
	}
	
	public static void main(String[] args){
		System.out.println(new MySolutionUsingdfs().subsets(new int[]{1,2}));
	}
}
