package problem78_Subsets;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MySolution {
	public List<List<Integer>> subsets(int[] nums) {
		List<List<Integer>> result=new LinkedList<>();
		result.add(new LinkedList<>());
		for(int num:nums){
			int size=result.size();
			for(int i=0;i<size;i++){
				List<Integer> copy=new ArrayList<>(result.get(i));
				copy.add(num);
				result.add(copy);
			}
		}
		return result;
    }
	
	public static void main(String[] args){
		System.out.println(new MySolution().subsets(new int[]{1,2,3}));
	}
}
