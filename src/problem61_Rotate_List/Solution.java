package problem61_Rotate_List;

public class Solution {
    public ListNode rotateRight(ListNode head, int k) {
    	if(head==null) return head;
        int size=1;ListNode current=head;
        while(current.next!=null){
        	size++;
        	current=current.next;
        }
        current.next=head;
        int step=size-k%size;ListNode p=head;
        while(step-->1){
        	p=p.next;
        }
        head=p.next;
        p.next=null;
        return head;
    }
    
    public class ListNode {
    	      int val;
    	      ListNode next;
    	      ListNode(int x) { val = x; }
    	  }
}
