/**
 *Merge two sorted linked lists and return it as a new list. The new list 
 *should be made by splicing together the nodes of the first two lists. 
 */
/**
 * @author Ivan
 *
 */
package problem21_Merge_Two_Sorted_Lists;