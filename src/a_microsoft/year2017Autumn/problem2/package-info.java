/**
 * Alice writes an English composition with a length of N characters. However, her teacher requires that
 * M illegal pairs of characters cannot be adjacent, and if 'ab' cannot be adjacent, 'ba' cannot be adjacent either.
 * In order to meet the requirements, Alice needs to delete some characters.
 * Please work out the minimum number of characters that need to be deleted.
 * 
 * Input:The first line contains the length of the composition N.
 * The second line contains N characters, which make up the composition. Each character belongs to 'a'..'z'.
 * The third line contains the number of illegal pairs M.
 * Each of the next M lines contains two characters ch1 and ch2,which cannot be adjacent.  
 * 
 * Output:One line with an integer indicating the minimum number of characters that need to be deleted.
 * Sample Hint:Delete 'a' and 'd'.
 * Sample Input:
 * 5
 * abcde
 * 3
 * ac
 * ab
 * de
 * Sample Output:
 * 2
 */
/**
 * @author I321035
 *
 */
package a_microsoft.year2017Autumn.problem2;