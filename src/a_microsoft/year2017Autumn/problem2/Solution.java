package a_microsoft.year2017Autumn.problem2;

import java.util.Scanner;

import a_microsoft.year2017Autumn.problem3.Helper;

/**
 * dp[ch]表示以ch结尾的字符串的最长长度
 * 那么dp[ch]=math.max(dp[ch],dp[x]+1)  x between 'a'-'z' 
 * 
 * @author I321035
 *
 */
public class Solution {
	private static boolean[][] conflictsTable=new boolean[26][26];
	
	
	public static void main(String[] args){
		Helper.generateInputStream("5","abcde","3","ac","ab","de");
		Scanner scanner =new Scanner(System.in);
		scanner.next();
		String input=scanner.next();
		int m=scanner.nextInt();
		for(int i=0;i<m;i++){
			char[] cs=scanner.next().toCharArray();
			conflictsTable[cs[0]-'a'][cs[1]-'a']=true;
			conflictsTable[cs[1]-'a'][cs[0]-'a']=true;
		}
		char[] arr=input.toCharArray();
		int[] dp=new int[26];
		for(int i=0;i<arr.length;i++){
			char c=arr[i];
			int tmp=1;
			for(int j=0;j<26;j++){
				if(conflictsTable[j][c-'a'])
					continue;
				tmp=Math.max(tmp, dp[j]+1);
			}
			dp[c-'a']=tmp;
		}
		int max=0;
		for(int i=0;i<26;i++){
			max=Math.max(max, dp[i]);
		}
		System.out.println(arr.length-max);
		scanner.close();
	}
	
	
}
