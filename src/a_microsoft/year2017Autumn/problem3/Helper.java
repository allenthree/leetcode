package a_microsoft.year2017Autumn.problem3;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Helper {
	private static String insertLineSeparator(String[] lines) {
		StringBuilder sb = new StringBuilder();
		for (String line : lines) {
			sb.append(line + System.getProperty("line.separator"));
		}
		return sb.toString();
	}
	
	public static void generateInputStream(String... lines){
		String input=insertLineSeparator(lines);
		InputStream in=new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
	}
}
