/**
 * It's H University's Registration Day for new students. There are M offices in H University, numbered from 1 to M. 
 * Students need to visit some of them in a certain order to finish their registration procedures. 
 * The offices are in different places. So it takes K units of time to move from one office to another.
 * There is only one teacher in each office. It takes him/her some time to finish one student's procedure. 
 * For different students this time may vary. At the same time the teacher can only serve one student so some students 
 * may need to wait outside until the teacher is available. Students who arrived at the office earlier will be served
 * earlier. If multiple students arrived at the same time they will be served in ascending order by student number.
 * N new students need to finish his/her registration. They are numbered from 1 to N. The ith student's 
 * student number is Si. He will be arrived at H University's gate at time Ti. He needs to visit Pi offices 
 * in sequence which are Oi,1, Oi,2, ... Oi,Pi. It takes him Wi,1, Wi,2, ... Wi,Pi units of time to finish the 
 * procedure in respective offices. It also takes him K units of time to move from the gate to the first office.
 * For each student can you tell when his registration will be finished? 
 * 
 * Input:
 * The first line contains 3 integers, N, M and K. (1 <= N <= 10000, 1 <= M <= 100, 1 <= K <= 1000)
 * The following N lines each describe a student.
 * For each line the first three integers are Si, Ti and Pi. Then following Pi pairs of integers: 
 * Oi,1, Wi,1, Oi,2, Wi,2, ... Oi,Pi, Wi,Pi. (1 <= Si <= 2000000000, 1 <= Ti <= 10000, 
 * 1 <= Pi <= M, 1 <= Oi,j <= M, 1 <= Wi,j <= 1000)
 * Output:
 * For each student output the time when he finished the registration.
 * Sample Input
 * 2 2 100  
 * 1600012345 500 2 1 500 2 500  
 * 1600054321 1100 1 2 300
 * Sample Output
 * 1700  
 * 2000
 * 
 */
/**
 * @author I321035
 *
 */
package a_microsoft.year2017Autumn.problem3;