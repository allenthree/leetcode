package problem225_Implement_Stack_using_Queues;

import java.util.LinkedList;
import java.util.Queue;

/**
 * pop and peek method is insufficient
 * 
 * @author john
 *
 */
public class MyStackUsing2Queues2 {
	Queue<Integer> q1=new LinkedList<>();
	Queue<Integer> q2=new LinkedList<>();
	
	public void push(int x) {
		Queue<Integer> in=q1.isEmpty()?q2:q1;
		in.add(x);
	}

	public int pop() {
		Queue<Integer> out=q1.isEmpty()?q2:q1;
		Queue<Integer> in=q1.isEmpty()?q1:q2;
		transfer(out, in);
		return out.poll();
	}

	public int top() {
		Queue<Integer> out=q1.isEmpty()?q2:q1;
		Queue<Integer> in=q1.isEmpty()?q1:q2;
		transfer(out, in);
		in.add(out.peek());
		return out.poll();
	}

	public boolean empty() {
		return q1.isEmpty()&&q2.isEmpty();
	}
	
	private void transfer(Queue<Integer> src,Queue<Integer> dest){
		int size=src.size();
		for(int i=1;i<size;i++){
			dest.add(src.poll());
		}
	}
}
