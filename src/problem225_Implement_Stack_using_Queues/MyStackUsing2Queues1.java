package problem225_Implement_Stack_using_Queues;

import java.util.LinkedList;
import java.util.Queue;

/**
 * push method is insufficient
 * 
 * @author john
 *
 */
public class MyStackUsing2Queues1 {
	Queue<Integer> q1=new LinkedList<>();
	Queue<Integer> q2=new LinkedList<>();
	
	public void push(int x) {
		Queue<Integer> out=q1.isEmpty()?q2:q1;
		Queue<Integer> in=q1.isEmpty()?q1:q2;
		in.add(x);
		transfer(out, in);
	}

	public int pop() {
		return q1.isEmpty()?q2.poll():q1.poll();
	}

	public int top() {
		return q1.isEmpty()?q2.peek():q1.peek();
	}

	public boolean empty() {
		return q1.isEmpty()&&q2.isEmpty();

	}
	
	private void transfer(Queue<Integer> src,Queue<Integer> dest){
		while(!src.isEmpty()){
			dest.add(src.poll());
		}
	}
}
