package problem62_UniquePaths;

public class MySolutionUsingRecursive {
	//m and n will be at most 100.
	public int uniquePaths(int m, int n) {
		return uniquePaths(1,1,m,n);
    }
	
	private int uniquePaths(int startI,int startJ,int m,int n){
		if(startI>n||startJ>m)
			return 0;
		else if(startJ==m&&startI==n)
			return 1;
		return uniquePaths(startI+1,startJ,m,n)+uniquePaths(startI,startJ+1,m,n);
	}
	
	public static void main(String[] args){
		System.out.println(new MySolutionUsingRecursive().uniquePaths(100, 100));
	}
}
