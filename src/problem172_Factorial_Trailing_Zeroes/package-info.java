/**
 * Given an integer n, return the number of trailing zeroes in n!.
 * Note: Your solution should be in logarithmic time complexity.
 */
/**
 * @author I321035
 *
 */
package problem172_Factorial_Trailing_Zeroes;