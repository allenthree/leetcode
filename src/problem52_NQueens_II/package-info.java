/**
 * Follow up for N-Queens problem.
 * Now, instead outputting board configurations, return the total number of distinct solutions.
 */
/**
 * @author john
 *
 */
package problem52_NQueens_II;