package problem50_Pow;

public class MySolution {
	public double myPow(double x, int n) {
		if(n==Integer.MIN_VALUE)
			if(n==Integer.MIN_VALUE) return myPow(x,-1)*myPow(x,Integer.MIN_VALUE+1);
		double factor=x,result=1.0;
		int p=Math.abs(n);
		while(p>0){
			if((p&1)==1)
				result*=factor;
			factor*=factor;
			p>>=1;
		}
		return n<0?1/result:result;
    }
	
	public static void main(String[] args){
		System.out.println(new MySolution().myPow(2.0, 4));
	}
}
