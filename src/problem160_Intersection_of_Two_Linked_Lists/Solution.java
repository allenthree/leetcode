package problem160_Intersection_of_Two_Linked_Lists;

import problem82_Remove_Duplicates_from_Sorted_List2.ListNode;

public class Solution {
	 public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
		 if(headA==null||headB==null) return null;
		 ListNode endA=null,endB=null,pA=headA,pB=headB;
	        while(pA!=pB){
	        	if(pA.next==null){
	        		endA=pA;
	        		pA=headB;
	        	}else{
	        		pA=pA.next;
	        	}
	        	if(pB.next==null){
	        		endB=pB;
	        		pB=headA;
	        	}else{
	        		pB=pB.next;
	        	}
	        	if(endA!=null&&endB!=null&&endA!=endB)
	        		return null;
	        }
	        return pA;
	 }
}
