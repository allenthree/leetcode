/**
 * 9. Palindrome Number
 * Determine whether an integer is a palindrome. Do this without extra space.
 */
/**
 * @author I321035
 *
 */
package problem9_Palindrome_Number;