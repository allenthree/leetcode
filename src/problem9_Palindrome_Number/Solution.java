package problem9_Palindrome_Number;

public class Solution {
	public boolean isPalindrome(int x) {
		if(x<0) return false;
		int length=1;
		for(length=1;x/length>=10;length*=10);
		while(x!=0){
			int left=x/length;
			int right=x%10;
			if(left!=right) return false;
			x=x%length/10;
			length/=100;
		}
		return true;
	}
	
	public static void main(String[] args){
		System.out.println(new Solution().isPalindrome(23442));
	}
}
