package problem111_Minimum_Depth_of_Binary_Tree;

import problem108_Convert_Sorted_Array_to_Binary_Search_Tree.TreeNode;

public class Solution {
	public int minDepth(TreeNode root) {
		if(root==null)
			return 0;
		int left=minDepth(root.left);
		int right=minDepth(root.right);
		if(left==0)	return right+1;
		if(right==0) return left+1;
		return left<right?left+1:right+1;
	}
}
