package problem150_Evaluate_Reverse_Polish_Notation;

import java.util.Stack;

public class Main {
	public int evalRPN(String[] tokens) {
		Stack<String> stack=new Stack<>();
		for(String s:tokens){
			switch(s){
			case "+":
				stack.push(""+(toInt(stack.pop())+toInt(stack.pop())));break;
			case "-":
				int o1=toInt(stack.pop());
				int o2=toInt(stack.pop());
				stack.push(""+(o2-o1));break;
			case "*":
				stack.push(""+(toInt(stack.pop())*toInt(stack.pop())));break;
			case "/":
				int o11=toInt(stack.pop());
				int o21=toInt(stack.pop());
				stack.push(""+(o21/o11));break;
			default:
				stack.push(s);
			}
		}
		return toInt(stack.pop());
	}
	
	private int toInt(String s){
		return Integer.parseInt(s);
	}
	
	public static void main(String[] args){
		System.out.println(new Main().evalRPN(new String[]{"-1","1","*","-1","+"}));
	}
}
