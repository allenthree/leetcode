package problem34_Search_for_a_Range;

import java.util.Arrays;

public class MySolution {
	  public int[] searchRange(int[] nums, int target) {
		   return new int[]{findLeft(nums,target),findRight(nums, target)};
	    }
	   
	   private int findRight(int[] nums,int target){
		   int left=0,right=nums.length-1;
		   while(left<=right){
			   int mid=left+right>>1;
		   		if(nums[mid]<=target)
		   			left=mid+1;
		   		else
		   			right=mid-1;
		   }
		   return right>=0&&nums[right]==target?right:-1;
	   }
	   
	   private int findLeft(int[] nums,int target){
		   int left=0,right=nums.length-1;
		   while(left<=right){
			   int mid=left+right>>1;
		   		if(nums[mid]<target)
		   			left=mid+1;
		   		else
		   			right=mid-1;
		   }
		   return left<nums.length&&nums[left]==target?left:-1;
	   }
	public static void main(String[] args){
		System.out.println(Arrays.toString(new MySolution().searchRange(new int[]{2,2}, 3)));
	}
	
}
