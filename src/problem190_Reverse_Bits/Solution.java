package problem190_Reverse_Bits;

public class Solution {
	public int reverseBits(int n) {
		int k=0;
		for(int i=0;i<32;i++,n>>=1){
			k=(k<<1)|(n&1);
		}
		return k;
    }
}
