package problem268_Missing_Number;

public class Solution {
	public int missingNumber(int[] nums) {
		int res=0;
		for(int i=0;i<nums.length;i++){
			res^=(i+1)^nums[i];
		}
		return res;
	}
	
	public int missingNumberSolution2(int[] nums) {
		int expected=(nums.length+1)*nums.length/2;
		for(int i=0;i<nums.length;i++){
			expected-=nums[i];
		}
		return expected;
	}
	
	
}
