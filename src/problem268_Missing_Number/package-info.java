/**
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n,
 * find the one that is missing from the array.
 * 
 * For example,
 * 
 * Given nums = [0, 1, 3] return 2.
 */
/**
 * @author I321035
 *
 */
package problem268_Missing_Number;