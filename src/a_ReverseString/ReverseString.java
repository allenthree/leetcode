package a_ReverseString;

/**
 * 对于abc ,翻转为cba ,可以使用^操作节省空间。
 * 对于I love you 翻转为 you love I 。使用stack。
 * 
 * @author Ivan
 *
 */
public class ReverseString {
	public String reverse(String s){
		char[] src=s.toCharArray();
		for(int i=0;i<src.length/2;i++){
			swap(src,i,src.length-1-i);
		}
		return new String(src);
	}
	
	private void swap(char[] src, int i,int j){
		if(src[i]==src[j])
			return;
		src[i]^=src[j];
		src[j]^=src[i];
		src[i]^=src[j];
	}
	
	public static void main(String[] args){
		System.out.println(new ReverseString().reverse("abcd"));
	}
}
