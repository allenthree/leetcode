package a_ReverseString;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 对于I love   you 翻转为 you   love I 
 * 
 * @author Ivan
 *
 */
public class ReverseSentence {
	public String reverse(String s){
		char[] src=s.toCharArray();
		Queue<Character> queue=new LinkedList<>();
		Stack<String> stack=new Stack<>();
		for(int i=0;i<src.length;i++){
			if(src[i]==' '){
				stack.push(dequeue(queue));
				stack.push(" ");
			}else{
				queue.add(src[i]);
			}
		}
		stack.push(dequeue(queue));
		StringBuilder sb=new StringBuilder();
		while(!stack.isEmpty()){
			sb.append(stack.pop());
		}
		return sb.toString();
	}
	
	private String dequeue(Queue<Character> queue){
		StringBuilder sb=new StringBuilder();
		while(!queue.isEmpty()){
			sb.append(queue.poll());
		}
		return sb.toString();
	}
	
	public static void main(String[] args){
		System.out.println(new ReverseSentence().reverse("I love   you.    "));
	}
}
