package problem264_Ugly_Number_II;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	 public int nthUglyNumber(int n) {
		 List<Integer> res=new ArrayList<>();
		 res.add(1);
		 int i=0,j=0,k=0;
		 while(res.size()<n){
			 int m1=res.get(i)*2;
			 int m2=res.get(j)*3;
			 int m3=res.get(k)*5;
			 int min=Math.min(m1, Math.min(m2, m3));
			 if(min==m1)
				 i++;
			 if(min==m2)
				 j++;
			 if(min==m3)
				 k++;
			 res.add(min);
		 }
		 return res.get(res.size()-1);
	 }
}	
