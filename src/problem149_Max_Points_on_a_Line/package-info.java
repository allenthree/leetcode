/**
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
 */
/**
 * @author Ivan
 *
 */
package problem149_Max_Points_on_a_Line;